import React,{useState} from 'react'

export default function TextForm(props) {

    const handleUpcase=() =>
    {
        console.log("upeercase" + text);
        let newText=text.toUpperCase();
        setText(newText);

    }

    const handleChange=(event)=>
    {
        console.log("handle change");
        setText(event.target.value); //now we can able to write in textbox eg: the already written text+... we write will come 
    }
    const[text, setText] = useState(''); //set it to blank
    //text="hello" //wrong way
    //setText("hii") //correct way (its a function)
  return (
      <>
    <div>
        
        <h1>{props.heading}</h1>
    <textarea className="form-control" id="myBox" value={text} rows="8" onChange={handleChange}></textarea>
    <button className="btn btn-primary" onClick={handleUpcase}>Convert</button>
    </div>

    <div className='container'>
        <h1>
            Hello world
        </h1>
        <p>
            {text.split(" ").length} words {text.length} characters
        </p>

    </div>
    </>
  )
}
